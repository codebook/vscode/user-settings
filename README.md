# Sample VS Code User Settings


    {
        "window.zoomLevel": 2,
        "editor.tabSize": 2,
        "files.exclude": {
            "**/.git": true,
            "**/.svn": true,
            "**/.hg": true,
            "**/CVS": true,
            "**/.DS_Store": true,
            "**/*.ngfactory.ts": true,
            "**/*.ngsummary.json": true,
            "**/.vscode": true
        },
        "workbench.editor.openPositioning": "first",
        "explorer.autoReveal": false,
        "typescript.check.tscVersion": false
    }

